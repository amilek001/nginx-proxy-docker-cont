FROM python:2.7-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY ./weeman/ /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "/usr/src/app/weeman.py" ]

